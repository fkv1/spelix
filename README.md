# Spelix

This is just a bugtracker for www.spelix.at. I have no access to their source code, but created this bugtracker because there hadn't been one and they didn't know how to set one up. They have since been aware of this bugtracker, but have shown no desire to fix any of the bugs. That's one of the reasons why I started to write my own [cave database](https://gitlab.com/fkv1/biodb) which is already more powerful.
